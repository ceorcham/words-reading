/* eslint-env node */
const express = require('express');

const port = process.env.PORT || 8080;
const app = express();
app.use(express.static(__dirname));
app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info(
        '==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.',
        port,
        port
    );
});
