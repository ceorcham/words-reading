(function() {
  'use strict';

  var app = {
    isLoading: true,
    visibleCards: {},
    words: [],
    currentIndex: 0,
    spinner: document.querySelector('.loader'),
    card: document.querySelector('.card'),
    container: document.querySelector('.main'),
    addDialog: document.querySelector('.dialog-container')
  };

  function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
  }


  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

  document.getElementById('card').addEventListener('click', function() {
    // Close the add new city dialog
    app.showNextWord();
  });

  document.getElementById('butRefresh').addEventListener('click', function() {
    // Refresh all of the forecasts
    app.showRandomWord();
  });

  document.getElementById('butAdd').addEventListener('click', function() {
    // Open/show the add new city dialog
    app.toggleAddDialog(true);
  });

  document.getElementById('butFullScreen').addEventListener('click', function() {
    // Toggle full screen
    if(document.fullscreenElement)
      document.getElementById('card').exitFullscreen();
    else
      document.getElementById('card').requestFullscreen();
  });

  document.getElementById('butAddCity').addEventListener('click', function() {
    // Add the newly selected city
    var input = document.getElementById('wordToAdd');
    var word = input.value;
    if (!app.words) {
      app.words = [];
    }
    app.words.push(word);
    input.value = '';
    app.saveWords();
    app.toggleAddDialog(false);
  });

  document.getElementById('butAddCancel').addEventListener('click', function() {
    // Close the add new city dialog
    app.toggleAddDialog(false);
  });

  document.getElementById('card').addEventListener('touchstart', handleTouchStart, false);
  document.getElementById('card').addEventListener('touchmove', handleTouchMove, false);

  var xDown = null;
  var yDown = null;

  function getTouches(evt) {
    return evt.touches ||             // browser API
          evt.originalEvent.touches; // jQuery
  }

  function handleTouchStart(evt) {
      const firstTouch = getTouches(evt)[0];
      xDown = firstTouch.clientX;
      yDown = firstTouch.clientY;
  };

  function handleTouchMove(evt) {
      if ( ! xDown || ! yDown ) {
          return;
      }

      var xUp = evt.touches[0].clientX;
      var yUp = evt.touches[0].clientY;

      var xDiff = xDown - xUp;
      var yDiff = yDown - yUp;

      if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
          if ( xDiff > 0 ) {
              /* left swipe */
              app.showNextWord();
          } else {
              /* right swipe */
              app.showPrevWord();
          }
      } else {
          if ( yDiff > 0 ) {
            // up
            app.showNextWord();
          } else {
            // down
            app.showPrevWord();
          }
      }
      /* reset values */
      xDown = null;
      yDown = null;
  };


  /*****************************************************************************
   *
   * Methods to update/refresh the UI
   *
   ****************************************************************************/

  // Toggles the visibility of the add new city dialog.
  app.toggleAddDialog = function(visible) {
    if (visible) {
      app.addDialog.classList.add('dialog-container--visible');
      document.getElementById('wordToAdd').focus();
    } else {
      app.addDialog.classList.remove('dialog-container--visible');
    }
  };

  /*****************************************************************************
   *
   * Methods for dealing with the model
   *
   ****************************************************************************/

  // Save list of cities to localStorage.
  app.saveWords = function() {
    var words = JSON.stringify(app.words);
    //localStorage.words = words;
  };

  app.showNextWord = function() {
    if(app.currentIndex == app.words.length - 1)
      app.currentIndex = 0;
    else
      app.currentIndex++;

    app.card.innerHTML = app.words[app.currentIndex];
  };

  app.showPrevWord = function() {
    if(app.currentIndex == 0)
      app.currentIndex = app.words.length - 1;
    else
      app.currentIndex--;

    app.card.innerHTML = app.words[app.currentIndex];
  };

  app.showRandomWord = function() {
    shuffle(app.words);
    app.currentIndex = Math.floor(Math.random() * app.words.length);
    app.card.innerHTML = app.words[app.currentIndex];
  };

  /************************************************************************
   *
   * Code required to start the app
   *
   * NOTE: To simplify this codelab, we've used localStorage.
   *   localStorage is a synchronous API and has serious performance
   *   implications. It should not be used in production applications!
   *   Instead, check out IDB (https://www.npmjs.com/package/idb) or
   *   SimpleDB (https://gist.github.com/inexorabletash/c8069c042b734519680c)
   ************************************************************************/

  // TODO add startup code here
  app.words = localStorage.words;
  if (app.words) {
    app.words = JSON.parse(app.words);
  } else {
    /* The user is using the app for the first time, or the user has not
     * saved any cities, so show the user some fake data. A real app in this
     * scenario could guess the user's location via IP lookup and then inject
     * that data into the page.
     */
    /*
    app.words = [
      'Sofía', 'mamá', 'papá', 'casa', 'perro', 'gato',
      'luna', 'sol', 'feliz', 'navidad', 'árbol', 'flor',
      'cielo', 'escuela', 'César', 'caballo', 'oso',
      'sapo', 'baño', 'Patricia', 'planta', 'niño', 'niña'
    ];
    */
    fetch('https://docs.google.com/spreadsheets/d/e/2PACX-1vRpVMqj-X7B7N4zqFLH8BMhvD0kaOcvGyDS_RiHWKEZpdgjTgzG2KarpsWQj6hyTATeSQNIu8xb_uXh/pub?gid=0&single=true&output=csv')
    .then(function(response) {
      return response.text();
    })
    .then(function(myText) {
      app.words = myText.split("\n");
      app.card.innerHTML = app.words[app.currentIndex];
    });
    app.saveWords();
  }

  // Check that service workers are registered
  if ('serviceWorker' in navigator) {
    // Use the window load event to keep the page load performant
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/sw.js');
    });
  }
})();
